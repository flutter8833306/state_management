
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("in main build");
    return MultiProvider(  
      providers :[

        Provider( 
          create: (context){
            return PlayerInfo(playerName: "Virat",playerCon: "RCB"); 
          }
        ),
        ChangeNotifierProvider( 
          create:(context){
            return Match(matchNo: 200,score: 8000);
          }
        ),
      ] ,

      child: const MaterialApp(  
        home: HomePage(),
      ),
    );
      
  
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    log("in homepage build");
    return Scaffold(
      appBar: AppBar(title:const Text("Consumer"),),

      body:  Container(
        width: double.infinity,
        child: Column(  
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
        
          children: [  
        
            Text(Provider.of<PlayerInfo>(context).playerName),
        
            Text(Provider.of<PlayerInfo>(context).playerCon),
            
            Consumer(builder: (BuildContext , dynamic , ){
              return
            })
        
            Text("${Provider.of<Match>(context).matchNo}"),
        
            Text("${Provider.of<Match>(context).score}"),

            ElevatedButton(onPressed: (){

              Provider.of<Match>(context , listen : false).changeData(200,400);


            }, child: const Text("change Team ")),

             const NormalClass(),
          
          ],
        ),
      ),
    );
  }
}

class NormalClass extends StatefulWidget {
  const NormalClass({super.key});

  @override
  State<NormalClass> createState() => _NormalClassState();
}

class _NormalClassState extends State<NormalClass> {
  @override
  Widget build(BuildContext context) {
    log("Normal Class Build");
    return Text(Provider.of<PlayerInfo>(context).playerName);
  }
}
class Match extends ChangeNotifier {
   int matchNo ;
   int score ;

  Match({required this.matchNo , required this.score});

  void changeData(int matchNo , int score){
    this.matchNo = matchNo ;
    this.score = score ;
    notifyListeners();

  }
}


class PlayerInfo {
   String playerName ;
   String playerCon ;

  PlayerInfo({required this.playerName , required this.playerCon});

  void changePlayerData(String name , String contry){
   
      playerName = name ;
      playerCon = contry ;
  }
}