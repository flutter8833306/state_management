

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("In MainApp Build Method");
    return ChangeNotifierProvider(  
      create: (context){
        return PlayerData( playerName: "virat kohli" , playerTeam: "RCB " , jerNo: 18);
      },
      child: MaterialApp(  
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  State<HomePage> createState() {
    return _HomePageState() ;
  }
}

class _HomePageState extends State<HomePage>{  
  Widget build(BuildContext context){
    log('IN HomePage Build Method');
    return Scaffold(  
      appBar: AppBar(title: const Text("Change_Notifier_Provider"),),
      body:Center(  
        child: Column(  
          children: [  
            Text(Provider.of<PlayerData>(context).playerName),
            const SizedBox(height: 10,),
            Text("${Provider.of<PlayerData>(context).jerNo}"),
            const SizedBox(height: 10,),
            Text(Provider.of<PlayerData>(context).playerTeam),
            ElevatedButton(
              onPressed: (){
                Provider.of<PlayerData>(context , listen: false).changeData("CSK");

              },
              style: ButtonStyle(  
                backgroundColor: MaterialStatePropertyAll(Colors.pink.shade100)
              ),
              child: const Text("Change Team ")
              ),
              const NormalClass(),
          ],), ) );}
}
class NormalClass extends StatelessWidget {
  const NormalClass({super.key}); 

  @override
  Widget build(BuildContext context) {
    log(" IN NormalClass Builder Method");
    return const  Column(  
      children: [  

          Text("hello world"),
        //  Text(Provider.of<PlayerData>(context).playerTeam),
           NormalClass2()
        
      ],
    );
  }
}

class NormalClass2 extends StatefulWidget {
  const NormalClass2({super.key});

  @override
  State<NormalClass2> createState() => _NormalClass2State();
}

class _NormalClass2State extends State<NormalClass2> {
  @override
  Widget build(BuildContext context) {

    log("IN HomePage2 Build mathod");
    return Column(
      children: [
        GestureDetector(
          onTap: (){

              Navigator.push(context, MaterialPageRoute(builder: (context){
                return Asach();
              }));
          
            
          },
          child: const Text("Hello world")),
      
      ],
    );
  }
}

class Asach extends StatelessWidget {
  const Asach({super.key});

  @override
  Widget build(BuildContext context) {
    log("in Asach");
    return Column(  
      children: [  
       GestureDetector(
        onTap: (){
          int data = Provider.of<PlayerData>(context ,  listen: false).jerNo ;
          data++ ;
          Provider.of<PlayerData>(context , listen: false).changejerNO(data);
        },
        child: Container(
          height:70 ,
          width: 200,
          child: Text("${Provider.of<PlayerData>(context).jerNo}")))
      ],
    );
  }
}
class PlayerData extends ChangeNotifier{

  String playerName ;
  int jerNo ;
  String playerTeam ;

  PlayerData({ required this.playerName , required this.jerNo , required this.playerTeam});

  void changeData(String playerTeam){
    this.playerTeam = playerTeam ;
    notifyListeners();
  }
  void changejerNO(int  jerNo){
    this.jerNo = jerNo ;
    notifyListeners();
  }
}

