import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:proxy_provider/Controller/proxy_controller.dart';
import 'package:proxy_provider/Controller/change_notifier_controller.dart';

import 'dart:developer';
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
   log("in homepage Build Method");
    return MaterialApp(
      home: Scaffold( 
        appBar: AppBar( 
          title: const Text("Proxy_provider"),
        ),
      
        body: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Text(Provider.of<Employee>(context).userName),
            Text("${Provider.of<Employee>(context).password}"),
            Text(Provider.of<Employee>(context).employeeName),
            Text(Provider.of<Employee>(context).companyName),
            ElevatedButton(onPressed: (){
              Provider.of<User>(context , listen: false).changeUserData("pkpkpkpk", 9322884250);
            }, 
            child:const Text("data change ")
            )
         ],     
        ),
      ),
    );
  }
}