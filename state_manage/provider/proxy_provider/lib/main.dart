import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:proxy_provider/Controller/change_notifier_controller.dart';
import 'package:proxy_provider/Controller/proxy_controller.dart';
import 'package:proxy_provider/View/homePage_view.dart';
import 'dart:developer';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
   log(" in mainApp Build");
    
    return MultiProvider( 
      providers: [  

        ChangeNotifierProvider( 
          create: (context){
            log("in ChangeNotifier Provider");
            return User(
              userName: "prajwal",
              password: 9322884250
            );
          },
        ),

        ChangeNotifierProxyProvider<User,Employee>( 
          create: (context){
            log("in ProxyProvider Create Function");
            return Employee(companyName: "Biencaps",
             employeeName: "Prajwal",
              password: Provider.of<User>(context , listen: false).password, 
              userName: Provider.of<User>(context , listen: false).userName
              );},
             update: (context , User , update){
             log("in ProxyProvider Update Function");
           return Employee(companyName: "Biencaps",
              employeeName: "Prajwal",
              password: User.password, 
              userName: User.userName
              );},),],
      child: const  HomePage(),
    );
   
  }
}



  
