import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Provider(  
      create:(context){
        return ProviderClass(playerName: "Virat", jerNO: 18 , teamName: "RCB");
      } ,
      child: MaterialApp(  
        home: HomePage(),
      ),
    );
    

    

  }
}
class ProviderClass  {
    String playerName ;
    int jerNO ;
    String teamName ;
   ProviderClass({required this.playerName ,required this.jerNO  ,required this.teamName}) ;


   void data(String name ,int jer , String team){
    playerName = name ;
    jerNO = jer ;
    teamName  = team ;
    

   }


}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(  
        title: const Text("Provier"),
        centerTitle: true,
      ),

      body: Center(
        child: Column( 
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Container(  
              height: 30 ,
              width: 200,
              alignment: Alignment.center,
              color: Colors.pink.shade100,
              child: Text(Provider.of<ProviderClass>(context , listen: false).playerName),
            ) ,                                                                                                                                                             
              Container(  
              height: 30 ,
              width: 200,
              alignment: Alignment.center,
              color: Colors.pink.shade100,
              child: Text("${Provider.of<ProviderClass>(context ,listen: false ).jerNO}"),
            ) ,
             Container(  
              height: 30 ,
              width: 200,
              alignment: Alignment.center,
              color: Colors.pink.shade100,
              child: Text(Provider.of<ProviderClass>(context , listen: false ).teamName),
            ) ,

            const  SizedBox(  
              height: 10,

            ),
              GestureDetector(
                onTap: (){
                 // Provider.of<ProviderClass>(context , listen: false).teamName = "CSK";

                 Provider.of<ProviderClass>(context , listen: false ).data("rohit", 20, "pranav");


                

                },
                child: Container(  
                height: 50 ,
                width: 200,
                alignment: Alignment.center,
                color: Colors.pink.shade100,
                child: const Text("change team_name"),
                            ),
              ) 



          ],
        ),
      )

      

     );
  }
}