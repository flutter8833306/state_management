
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("in mainbuild");
     return ChangeNotifierProvider( 
      create: (context){
        return   EmpData(empName: "prajwal");
      },
      child:const MaterialApp( 
        home: HomePage(), 
        
      ) ,
     );
   
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

   String name = "prajwal";
  @override
  Widget build(BuildContext context) {
    log("in homepage build");
    return Scaffold(  
      appBar: AppBar(title: const Text("Change_Notifier_Provider"),),

      body: Container(
        width: double.infinity,
        child:   Column(  
          children: [ 

            Text(Provider.of<EmpData>(context).empName),
            ElevatedButton(onPressed: (){
              Provider.of<EmpData>(context , listen: false).changeData();
            }, child: const Text("change Text")),
             NormalClass1(nem: name),
        
          
          ],
        ),
      ),

    );
  }
}

class NormalClass1 extends StatelessWidget {
  final String nem ;
  const NormalClass1({super.key , required this.nem});

  @override
  Widget build(BuildContext context) {
    log("in normalClass1");
    return  Text(nem);
  }
}


class EmpData extends ChangeNotifier {

  final String empName ;
   EmpData({ required this.empName}) ;

   void changeData(){
     
     notifyListeners();
   }

}
