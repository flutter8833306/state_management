import 'package:ecom_state_management/model/laptopData.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class ShowData extends StatefulWidget {
   const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  @override
  Widget build(BuildContext context) {
    log("in show-Data build");
    return Scaffold(
      body: Column( 
        children: [  
          const SizedBox(height: 50,),
          Container(  
            height: 200,
            width: double.infinity,
            color: Colors.red.shade100,
            margin: const EdgeInsets.all(10),
            child: Row(  
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [ 
                
                
                Container(
                  
                  width: 100,
                  child: Image.asset(Provider.of<ProviderClass>(context).obj.productImg)),

                  Column( 
                    mainAxisAlignment: MainAxisAlignment.center,
                
                    children: [ 
                  
                    SizedBox(
                     width: 190,
                      child: Text(Provider.of<ProviderClass>(context).obj.productName , style: const  TextStyle(fontSize: 25),)),
                    SizedBox(
                      width: 200,
                      child: Text(" Price :${Provider.of<ProviderClass>(context , listen: false).obj.price}",style: const TextStyle( fontSize: 25))),
                    SizedBox(
                      width: 190,
                       child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [  
                          GestureDetector(
                            onTap: (){
                              bool val = Provider.of<ChangeNotifierClass>(context , listen: false).obj.isLike  ;
                              bool val2 = !val ;
                              Provider.of<ChangeNotifierClass>(context , listen: false).changeCountAndLike( Provider.of<ChangeNotifierClass>(context , listen: false).obj.likeCount,val2 );
                             
                             
                            },
                           child: Provider.of<ChangeNotifierClass>(context , listen: false).obj.isLike ? 
                                               const Icon(Icons.favorite ,color: Colors.red,) :
                                              const Icon(Icons.favorite_outline , size: 30,)),

                      
                      
                      
                        const  SizedBox(width: 40,),
                                        
                         // to minimize like count
                         GestureDetector(
                            onTap: (){
                              int likeCount= Provider.of<ChangeNotifierClass>(context , listen: false).obj.likeCount;
                              int likeCount2 = likeCount-1 ;
                              Provider.of<ChangeNotifierClass>(context , listen : false).changeCountAndLike(likeCount2 , false);
                             
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Colors.black ,
                                borderRadius: BorderRadius.circular(15)
                              ),
                              child:const Text("--" ,style: TextStyle(fontSize: 25 , color: Colors.white),)),
                          ),

                            const  SizedBox(width: 10,),


                            // to show the like count
                            Text("${Provider.of<ChangeNotifierClass>(context).obj.likeCount}" ,style: TextStyle(fontSize: 20),),
                           const  SizedBox(width: 10,),
                         
                         
                         
                         
                          // for add count
                           GestureDetector(
                            onTap: (){
                              int likes = Provider.of<ChangeNotifierClass>(context , listen: false).obj.likeCount ;
                              int like2 = likes+1 ;
                              Provider.of<ChangeNotifierClass>(context , listen : false).changeCountAndLike(like2, false);

                            },
                             child: Container(
                              height: 30,
                              width: 30,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Colors.black ,
                                borderRadius: BorderRadius.circular(17)
                              ),
                              child:const  Center(child: Text("+" ,style: TextStyle(fontSize: 25 , color: Colors.white),))),
                           ),
                       ],
                      ),
                     )    
                   ],
                 ),
                ]
              ),
            ),
          ],
        ),
      );
    }
  }