

import 'package:ecom_state_management/controller/list_controller.dart';
import 'package:ecom_state_management/controller/wishlist_controller.dart';
import 'package:ecom_state_management/model/product_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class Asach extends StatelessWidget{
  const Asach({super.key});

  @override
  Widget build(BuildContext context){
    return const  WishList();
  }
}

class WishList extends StatefulWidget {
  const WishList({super.key});

  @override
  State<WishList> createState() => _WishListState();
}

class _WishListState extends State<WishList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(  
        title:  const Text("WishList"),
       
      ),
       body:   ListView.builder(
        itemCount: Provider.of<WishListController>( context).wishList.length ,
        itemBuilder: (context , index){
          return  Container(  
              height: 200,
              width: double.infinity,
              color: Colors.red.shade100,
              margin: const EdgeInsets.all(10),
              child: Row(  
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [ 
                  
                  
                   Container(
                    
                    width: 100,
                    child: Image.asset(Provider.of<WishListController>(context).wishList[index].productImg)),
            
                    Column( 
                      mainAxisAlignment: MainAxisAlignment.center,
                  
                      children: [ 
                    
                      SizedBox(
                       width: 190,
                      child:
                       Text(Provider.of<WishListController>(context).wishList[index].productName , style: const  TextStyle(fontSize: 25),)
                       ),
                      SizedBox(
                        width: 200,
                        child: Text(" Price :${Provider.of<WishListController>(context).wishList[index].price}",style: const TextStyle( fontSize: 25))),
                      SizedBox(
                        width: 190,
                         child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [  
                            GestureDetector(
                              onTap: (){
                        
                              },
                             child: Provider.of<WishListController>(context , listen: false).wishList[index].isLike ? 
                                                 const Icon(Icons.favorite ,color: Colors.red,) :
                                                const Icon(Icons.favorite_outline , size: 30,)),
                            const  SizedBox(width: 20,),

                           GestureDetector(
                            onTap : (){

                              Provider.of<WishListController>(context , listen: false).removeFromWishList(
                                index: index ,
                                obj: Provider.of<WishListController>(context , listen: false).wishList[index]
                                 );
                              Provider.of<Productlist>(context , listen: false).update();
                            },
                            child: const Icon(Icons.delete_outline))
                         ],
                        ),
                       )    
                     ],
                   ),
                  ]
                ),
              );
        },

        ),
    );
    
  }
}