import 'dart:developer';

import 'package:ecom_state_management/model/laptopData.dart';
import 'package:ecom_state_management/view/show_detail.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GetProduct extends StatefulWidget {
  const GetProduct({super.key});

  @override
  State<GetProduct> createState() => _ProductState();
}

class _ProductState extends State<GetProduct> {
  TextEditingController img = TextEditingController();
  TextEditingController name1 = TextEditingController();
  TextEditingController price = TextEditingController();

  @override
  Widget build(BuildContext context) {
    log("in GetData");
    return
      Scaffold(
      body: Container(
        height: 500,
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  
               Container(
                
                margin: EdgeInsets.all(20),
                 child: TextField( 
                              controller: img, 
                              decoration:const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("Image"),
                      
                              ),
                            ),
               ),
                           Container(
                            margin: EdgeInsets.all(20),
                             child: TextField(  
                              controller: name1,
                              decoration: const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("name"),
                                               
                              ),
                                                     ),
                           ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: TextField( 
                                controller: price, 
                              decoration: const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("Price"),
                                                
                              ),
                                                      ),
                            ) ,
                            
                          ElevatedButton(onPressed: (){
        
                             int price1 = int.parse(price.text) ;
                             Provider.of<ProviderClass>(context , listen: false).getDataFromUser(price1 ,name1.text, img.text  );
        
                            //  Provider.of<ProviderClass>(context).obj.productImg = img.text ;
                            //  Provider.of<ProviderClass>(context).obj.productName = name1.text ;
        
                            Navigator.push(context , MaterialPageRoute(builder: (context){
                              return const  ShowData();
                            }));
        
                          }, child: Icon(Icons.add)),
                       
            ],
          ),
      )
    
    
    );
  }
}