import 'dart:developer';
import 'package:ecom_state_management/view/showdetail_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ecom_state_management/controller/list_controller.dart';
import 'package:ecom_state_management/model/product_model.dart';



class GetProduct extends StatefulWidget {
  const GetProduct({super.key});

  @override
  State<GetProduct> createState() => _ProductState();
}

class _ProductState extends State<GetProduct> {
  TextEditingController img = TextEditingController();
  TextEditingController name1 = TextEditingController();
  TextEditingController price = TextEditingController();

  @override
  Widget build(BuildContext context) {
    log("in GetData");
    return  Scaffold(
      body: Container(
        height: 500,
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  
               Container(
                
                margin: EdgeInsets.all(20),
                 child: TextField( 
                              controller: img, 
                              decoration:const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("Image"),
                      
                              ),
                            ),
               ),
                           Container(
                            margin: const EdgeInsets.all(20),
                             child: TextField(  
                              controller: name1,
                              decoration: const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("name"),
                                               
                              ),
                                                     ),
                           ),
                            Container(
                              margin:const EdgeInsets.all(20),
                              child: TextField( 
                                controller: price, 
                              decoration: const InputDecoration( 
                                border: OutlineInputBorder(),
                                label:Text("Price"),
                                                
                              ),
                                                      ),
                            ) ,
                            
                          ElevatedButton(onPressed: (){
        
                             int price1 = int.parse(price.text) ;
                             ProductData temp_obj = ProductData(
                              price: price1 ,
                              productImg: img.text ,
                              productName: name1.text ,
                              isLike: false,
                              quantity: 1 ,
                             

                             );
                             Provider.of<Productlist>(context , listen: false).addToProductList(obj: temp_obj);
     
                          }, child: const Text("Add"),
                        ),

                           
                          ElevatedButton(onPressed: (){
                            Navigator.push(context , MaterialPageRoute(builder: (context){
                              return const  ShowData();
                            }));
        
                          }, child: const Text("Show"),
                        )
                       
            ],
          ),
      )
    
    );
  }
}