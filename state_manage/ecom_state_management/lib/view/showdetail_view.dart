import 'package:ecom_state_management/controller/list_controller.dart';
import 'package:ecom_state_management/controller/wishlist_controller.dart';
import 'package:ecom_state_management/view/wishlist_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class ShowData extends StatefulWidget {
   const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  @override
  Widget build(BuildContext context) {
    log("in show-Data build");
    return Scaffold(
      appBar: AppBar( 
        actions:  [  
           GestureDetector(
            onTap: (){

              Navigator.of(context).push(MaterialPageRoute(builder: (context){
                return const Asach();
              }));
          
            },
            child: const Icon(Icons.favorite)) ,

           const  SizedBox(width: 50,)
        ],
      ),
      body: 
          
          Consumer(
            
            builder: (context , value , child){
              log("in Consumer");
              return ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: Provider.of<Productlist>(context).productList.length ,
              itemBuilder: (context , index){
               return Container(  
                height: 200,
                width: double.infinity,
                color: Colors.red.shade100,
                margin: const EdgeInsets.all(10),
                child: Row(  
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [ 
                    
                    
                     Container(
                      
                      width: 100,
                      child: Image.asset(Provider.of<Productlist>(context).productList[index].productImg)),
              
                      Column( 
                        mainAxisAlignment: MainAxisAlignment.center,
                    
                        children: [ 
                      
                        SizedBox(
                         width: 190,
                        child:
                         Text(Provider.of<Productlist>(context).productList[index].productName , style: const  TextStyle(fontSize: 25),)
                         ),
                        SizedBox(
                          width: 200,
                          child: Text(" Price :${Provider.of<Productlist>(context , listen: false).productList[index].price}",style: const TextStyle( fontSize: 25))),
                        SizedBox(
                          width: 190,
                           child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [  
                              GestureDetector(
                                onTap: (){
            
                                    Provider.of<Productlist>(context , listen: false).isLikedd(
                                    obj: Provider.of<Productlist>(context , listen: false).productList[index] ,
                                   
                                    );
                                 
                                    Provider.of<WishListController>(context , listen: false).addToWishList(
                                    obj:Provider.of<Productlist>(context , listen: false).productList[index],
                                    );
            
                                 
                                },
                               child: Provider.of<Productlist>(context , listen: false).productList[index].isLike ? 
                                                   const Icon(Icons.favorite ,color: Colors.red,) :
                                                  const Icon(Icons.favorite_outline , size: 30,)),
              
                          
                          
                          
                            const  SizedBox(width: 40,),
                                            
                             // to minimize like count
                             GestureDetector(
                                onTap: (){
                                 
                                   Provider.of<Productlist>(context , listen : false).decreaseCount(
                                    obj:Provider.of<Productlist>(context , listen: false).productList[index],
                                     );
                                 
                                },
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colors.black ,
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                  child:const Text("--" ,style: TextStyle(fontSize: 25 , color: Colors.white),)),
                              ),
              
                                const  SizedBox(width: 10,),
              
              
                                // to show the like count
                                Text("${Provider.of<Productlist>(context).productList[index].quantity}" ,style:const TextStyle(fontSize: 20),),
                               const  SizedBox(width: 10,),
                             
                             
                             
                             
                              // for add count
                               GestureDetector(
                                onTap: (){
                                  
                                  Provider.of<Productlist>(context , listen : false).increseCount(
                                    obj:Provider.of<Productlist>(context , listen: false).productList[index],
                                     );
              
                                },
                                 child: Container(
                                  height: 30,
                                  width: 30,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colors.black ,
                                    borderRadius: BorderRadius.circular(17)
                                  ),
                                  child:const  Center(child: Text("+" ,style: TextStyle(fontSize: 25 , color: Colors.white),))),
                               ),
                           ],
                          ),
                         )  ,
                        
                       ],
                        
                     ),
                   
                    ]
                  ),
                );
              }
            );
            }
          ),
          
        
      );
    }
  }