

import 'package:flutter/material.dart';
class ProductData {
  String  productImg ;
  String productName ;
  int price ;
  ProductData({required this.price , required this.productImg , required this.productName});

}

class ProductDetail {

  int likeCount  ;
  bool isLike ;
  ProductDetail({required this.isLike , required this.likeCount});

}



class ProviderClass extends ChangeNotifier {
   ProductData obj ;
   ProviderClass({required this.obj});

   void getDataFromUser( int price , String name , String img){
    obj.price = price ;
    obj.productName = name ;
    obj.productImg  = img ;
    notifyListeners();
   }

}
class ChangeNotifierClass extends ChangeNotifier {
  
   ProductDetail obj ;
   ChangeNotifierClass({required this.obj});
   void changeCountAndLike( int like , bool Like){
   obj.likeCount = like ;
   obj.isLike = Like ;
   notifyListeners();
   }

}