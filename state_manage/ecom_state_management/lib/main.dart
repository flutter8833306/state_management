import 'dart:developer';

import 'package:ecom_state_management/controller/list_controller.dart';
import 'package:ecom_state_management/controller/wishlist_controller.dart';

import 'package:ecom_state_management/view/getproduct_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("in mainApp Build");
    return  MultiProvider(
     
      providers:[
     
       ChangeNotifierProvider(
          create : (context){
          return  Productlist(productList: []);
        },) ,

       ChangeNotifierProvider(
        create: (context){
          return WishListController();
        }  
       )

      ],
        builder:(context, child) {
          return const MaterialApp( 
            home: GetProduct(),
          );
        },
      );
    
    
  }
}
