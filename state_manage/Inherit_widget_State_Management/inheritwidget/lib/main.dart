import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Input(),
    );
  }
}

class Input extends StatefulWidget {
  const Input({super.key});

  @override
  State<Input> createState() => _InputState();
}

class _InputState extends State<Input> {

  TextEditingController id1 = TextEditingController();
     TextEditingController name2 = TextEditingController();
  TextEditingController userName3 = TextEditingController();
    TextEditingController skill = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [  

             TextField( 
                          controller: id1, 
                          decoration:const InputDecoration( 
                            border: OutlineInputBorder(),
                            label:Text("id"),
                  
                          ),
                        ),
                         TextField(  
                          controller: name2,
                          decoration: const InputDecoration( 
                            border: OutlineInputBorder(),
                            label:Text("Name"),
                  
                          ),
                        ),
                          TextField( 
                            controller: userName3, 
                          decoration: const InputDecoration( 
                            border: OutlineInputBorder(),
                            label:Text("UserName"),
                  
                          ),
                        ) ,
                        ElevatedButton(onPressed: (){

                          Navigator.push(context , MaterialPageRoute(builder: (context){
                            return EmployeeData(id:int.parse(id1.text), name: name2.text, userName: userName3.text,child:ShowData());
                          }));

                        }, child: Icon(Icons.add))


          ],
        )
      );
  }
}


class ShowData extends StatefulWidget {
  const ShowData({super.key});

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  @override
  Widget build(BuildContext context) {
        EmployeeData data = EmployeeData.of(context);
    return Scaffold(appBar:AppBar(),
    body: Column(   
        children: [  
          
          Container(  
            height: 90,
            width: double.infinity,
            margin:const EdgeInsets.all(20),
            padding: const  EdgeInsets.all(30),
            color: Colors.pink.shade100,
            alignment: Alignment.center,
            child: Text("${data.id}"),
          ),
            Container(  
            height: 90,
            width: double.infinity,
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(30),
            color: Colors.pink.shade100,
            alignment: Alignment.center,
            child: Text("${data.name}"),
          ),
            Container(  
            height: 90,
            width: double.infinity,
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(30),
            color: Colors.pink.shade100,
            alignment: Alignment.center,
            child: Text("${data.userName}"),
          ),
          ElevatedButton(onPressed: (){

            showModalBottomSheet(context: context, builder: (context){

              return Padding(padding: MediaQuery.of(context).viewInsets ,
              child: Column( 
                mainAxisSize: MainAxisSize.min, 
                children: [  
                              
                     
                          TextField( 
                            //controller: userName3, 
                          decoration: InputDecoration( 
                            border: OutlineInputBorder(),
                            label:Text("UserName"),
                  
                          ),
                        ) ,
                        ElevatedButton(onPressed: (){

                        }, 
                        child: Text("submit"))
                ],
              ),

              );

            });

          }, child: Text("Add Skill")
          )
      
      
        ],
      
    ),
    );
  }
}



class EmployeeData extends InheritedWidget {

  int id ;
   String name ;
   String userName ;

   EmployeeData({super.key , required this.id , required this.name  , required this.userName ,required super.child});

 @override
 bool  updateShouldNotify(EmployeeData oldWidget){
    return id!= oldWidget.id ;
 }

 static  EmployeeData of(BuildContext context){

  return context.dependOnInheritedWidgetOfExactType()! ;

 }


  
}