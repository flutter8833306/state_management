
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center, 
            children: [ 

            Courses(coursesName: "java") ,
            SizedBox(height: 10,),
            Courses(coursesName: "python",)

            ],
          ),
        ),
      ),
    );
  }
}

class Courses extends StatefulWidget {

  String coursesName ;
  
  Courses({super.key ,required this.coursesName});

  @override
  State<Courses> createState() => _CoursesState();
}

class _CoursesState extends State<Courses> {

  int counter = 0 ;

  void increment(){
    setState((){
      counter++ ;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Row(  
      children: [  
        GestureDetector(
          onTap: (){
            increment();
          },
          child: Container(  
            alignment: Alignment.center,
            height: 100,
            width: 100,
            color:Colors.pink.shade200,
            child: Text(widget.coursesName),
          ),
        ) ,

        SizedBox(width: 20,),

        Container(  
          alignment: Alignment.center,
          height: 100,
          width: 100,
          color: Colors.pink.shade100,
          child: Text("$counter"),
        )
      ],
    );
  }
}