import 'package:assignment_employee_data/main.dart';
import 'package:flutter/material.dart';

class ShowSkill extends StatefulWidget {
  const ShowSkill({super.key});

  @override
  State<ShowSkill> createState() => _ShowSkillState();
}

class _ShowSkillState extends State<ShowSkill> {
  @override
  Widget build(BuildContext context) {

    EmployeeData obj = EmployeeData.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Skill"),

      ),
      body: ListView.builder(  
        itemCount : obj.data.skill.length ,
        itemBuilder : (context , index){
          return Container(
            height: 60,
            color: Colors.pink.shade100,
            width: double.infinity,
            margin:const EdgeInsets.all(20),
            alignment: Alignment.center,
            child: Text(obj.data.skill[index]  , style: const TextStyle(fontSize: 22),),
          );
        }
      ),

    );
  }
}