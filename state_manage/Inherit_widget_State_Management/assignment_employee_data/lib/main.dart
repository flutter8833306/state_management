import 'package:assignment_employee_data/modalclass.dart';
import 'package:assignment_employee_data/secondScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart%20';

void main() {
  runApp( MainApp());
}




class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return EmployeeData(
      data:  Data(0, ' ', ' ' , [ ]), 
      child: MaterialApp(
        home: Login(),
      ),
   );
  }
}

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {

  TextEditingController id1 = TextEditingController();
  TextEditingController name1 = TextEditingController();
  TextEditingController userName1 = TextEditingController();
  TextEditingController skill = TextEditingController();



  @override
  Widget build(BuildContext context) {
   EmployeeData obj = EmployeeData.of(context);
    return Scaffold(
      body: Column( 
        mainAxisAlignment: MainAxisAlignment.center,
        children: [  
             Container(
              margin: EdgeInsets.all(20),
               child: TextField( 
                            controller: id1, 
                            decoration:const InputDecoration( 
                              border: OutlineInputBorder(),
                              label:Text("id"),
                    
                            ),
                          ),
             ),
                         Container(
                          margin: EdgeInsets.all(20),
                           child: TextField(  
                            controller: name1,
                            decoration: const InputDecoration( 
                              border: OutlineInputBorder(),
                              label:Text("Name"),
                                             
                            ),
                                                   ),
                         ),
                          Container(
                            margin: EdgeInsets.all(20),
                            child: TextField( 
                              controller: userName1, 
                            decoration: const InputDecoration( 
                              border: OutlineInputBorder(),
                              label:Text("UserName"),
                                              
                            ),
                                                    ),
                          ) ,
                        ElevatedButton(onPressed: (){

                          obj.data.id = int.parse(id1.text);
                          obj.data.name = name1.text ;
                           obj.data.userName = userName1.text;
                          




                          Navigator.push(context , MaterialPageRoute(builder: (context){
                            return ShowData();
                          }));

                        }, child: Icon(Icons.add))


          ],
        )
      );
    
}
}

class EmployeeData extends InheritedWidget {

  Data data ;

   EmployeeData({super.key , required this.data ,required super.child});

 @override
 bool  updateShouldNotify(EmployeeData oldWidget){
    return data!= oldWidget.data ;
 }

 static  EmployeeData of(BuildContext context){

  return context.dependOnInheritedWidgetOfExactType()! ;

 }


  
}