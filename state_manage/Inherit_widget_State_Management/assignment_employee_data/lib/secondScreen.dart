
import 'package:assignment_employee_data/addSkill.dart';
import 'package:flutter/material.dart';
import 'package:assignment_employee_data/main.dart';

    

class ShowData extends StatefulWidget {
  const ShowData({super.key});
  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {

  TextEditingController skill = TextEditingController();


  @override
  Widget build(BuildContext context) {
    EmployeeData obj = EmployeeData.of(context);
    return 
    Scaffold(
      resizeToAvoidBottomInset : false ,
      body: Column( 
        mainAxisAlignment: MainAxisAlignment.center,  
          children: [  
            
            Container(  
              height: 90,
              width: double.infinity,
              margin:const EdgeInsets.all(20),
              padding: const  EdgeInsets.all(30),
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text("${obj.data.id}"),
            ),
              Container(  
              height: 90,
              width: double.infinity,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(30),
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text("${obj.data.name}"),
            ),
              Container(  
              height: 90,
              width: double.infinity,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(30),
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text("${obj.data.userName}"),
            ),

            ElevatedButton(onPressed: (){

              showModalBottomSheet( 
                isScrollControlled:true,
                context: context ,
                builder: (context){
                  return Padding(

                    padding: MediaQuery.of(context).viewInsets,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [  
                        SizedBox(height: 30,),
                        TextField(
                          controller: skill,
                          decoration:const InputDecoration(
                            label : Text("Add Skills here"),  
                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(15)))
                          ),
                          
                        ),
                        ElevatedButton(onPressed: (){
                          obj.data.skill.add(skill.text);
                          Navigator.of(context).pop();

                        }, child: Text("Add S")),
                        SizedBox(height: 50,),

                       
                      ],

                    ),
                  );

                },

              );

            },
            style: const ButtonStyle(  
              fixedSize: MaterialStatePropertyAll(Size(300,50))
            ),
             child: Text("Add SKill")
            
            ),
            SizedBox(height: 20,),
             ElevatedButton(onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context){
                            return ShowSkill();
                          }));
                        },
                        style:const  ButtonStyle(  
                          fixedSize: MaterialStatePropertyAll(
                            Size(300, 50)
                            
                          )
                        ),
                         child: Text("Show Skill"))
        
        
          ],
        
      ),
    );
  }
}

