import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  const PlayerData( 
      playerName : "prajwal",
      jerNo :18 ,
      playerTeam : "rcb" ,
      child: MaterialApp(  
        home: MyHomePage(title: "hello"),
      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

 
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 
  @override
  Widget build(BuildContext context) {

    return Scaffold(  
      appBar: AppBar(  
        title : Text("INherit Widget"),
      ),
      body: Column( 
        mainAxisAlignment: MainAxisAlignment.center, 
        children: [   
          Text()
        ],
      )
    );
   
   
  }
}

class PlayerData extends InheritedWidget {
  final String playerName ;
  final int jerNo ;
  final String playerTeam ;

  const PlayerData({super.key , required this.playerName , required this.jerNo , required this.playerTeam , required super.child});

 @override
  bool updateShouldNotify( PlayerData oldWidget){
    return playerTeam != oldWidget.playerTeam ;
  }

  PlayerData of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType()! ;
  }
} 


