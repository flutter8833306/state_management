import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}



class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}
String playerName = "virat kohli";
int playerJerNO = 18;
String playerTeam = "RCB";
class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return PlayerInfo(
      playerName: playerName,
      playerJerNO: playerJerNO,
      playerTeam: playerTeam,
      child:  MaterialApp(
        home: Scaffold(  
          appBar: AppBar(
        title: const Text("Player_Info"),
      ), 
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [  
          const HomeClass(),

            GestureDetector(
              onTap: () {
               
                setState(() {
                   playerTeam = "CSK";
                });
              },
              child: Container(
                  height: 50,
                  width: 200,
                  color: const Color.fromARGB(255, 140, 139, 139),
                  alignment: Alignment.center,
                  child: const Text(
                    "Change Ipl Team ",
                    style: TextStyle(fontSize: 22),
                  )),
            )

        ],
      ),
        ),
      ),
    );
  }
}

class HomeClass extends StatelessWidget {
  const HomeClass({super.key});

  @override
  Widget build(BuildContext context) {
     PlayerInfo obj = PlayerInfo.of(context);

    return Column(
    
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         
          children: [
            Container(
              height: 50,
              width: 250,
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text(
                obj.playerName,
                style: const TextStyle(fontSize: 22),
              ),
            ),
            SizedBox(height: 15,),
        
          
            Container(
              height: 50,
              width: 250,
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text(
                "${obj.playerJerNO}",
                style: const TextStyle(fontSize: 22),
              ),
            ),

            SizedBox(height: 15,),

            Container(
              height: 50,
              width: 250,
              color: Colors.pink.shade100,
              alignment: Alignment.center,
              child: Text(
                obj.playerTeam,
                style: const TextStyle(fontSize: 22),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
          
          ],
        
      
    );
  }
}

class PlayerInfo extends InheritedWidget {
   String playerName;
   int playerJerNO;
   String playerTeam;

  static PlayerInfo of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType()!;
  }

   PlayerInfo(
      {super.key,
      required this.playerName,
      required this.playerJerNO,
      required this.playerTeam,
      required super.child});

  
  bool updateShouldNotify(PlayerInfo oldWidget) {
    return playerTeam != oldWidget.playerTeam;
  }
}
