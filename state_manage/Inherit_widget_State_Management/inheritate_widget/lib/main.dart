import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}


class SharedData extends InheritedWidget {
  
  final String nameOfProduct ;
  final int price  ;
  const SharedData({ super.key ,required this.nameOfProduct,required this.price ,required super.child });
  
  static SharedData of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<SharedData>()! ;
  }

  
  
  @override
  bool updateShouldNotify( SharedData oldWidget){
    return price != oldWidget.price ;
  }



}
class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {

    return const  SharedData(nameOfProduct: "airpod",
           price: 300 ,

           child: MaterialApp( 

            home: Class2(),
            
           )
           ); 
  }
}

class Class2 extends StatefulWidget {
  const Class2({super.key});

  @override
  State<Class2> createState() => _Class2State();
}

class _Class2State extends State<Class2> {

   
  @override
  Widget build(BuildContext context) {
     SharedData SharedDataObj = SharedData.of(context); 
    return Scaffold(  
              appBar: AppBar(  
                title: const  Text("Inherit Widget"),
              ),
              body: Column(
                children: [  
                  Row(
                    children: [
                      Container(  
                        height: 100,
                        width: 100,
                        color: Colors.pink.shade100,
                        alignment: Alignment.center,
                        child:  Text(SharedDataObj.nameOfProduct),
                      ),
                
                   const SizedBox(width: 10,),

                    
                      Container(  
                        height: 100,
                        width: 100,
                        color: Colors.pink.shade100,
                        alignment: Alignment.center,
                        child: Text("${SharedDataObj.price}"),
                      ),
                      const  SizedBox( width: 10,),
                      
                    ],
                  ) , 

                const  SizedBox(height: 10,),
                const  SecondProduct(),
                ],
              ),
            );
  }
}



class SecondProduct extends StatefulWidget {
  const SecondProduct({super.key});

  @override
  State<SecondProduct> createState() => _SecondProductState();
}

class _SecondProductState extends State<SecondProduct> {
  @override
  Widget build(BuildContext context) {

    SharedData SharedAppData = SharedData.of(context);

    return Row(
      children: [
        Container( 
          alignment: Alignment.center,
          height: 100,
          width: 100,
          color: Colors.blue.shade100 ,
          child: Text(SharedAppData.nameOfProduct),
        ),
        SizedBox(width: 10,),
         Container( 
          alignment: Alignment.center,
          height: 100,
          width: 100,
          color: Colors.blue.shade100 ,
          child: Text("${SharedAppData.price}"),
        ),
      ],
    );
  }
}
